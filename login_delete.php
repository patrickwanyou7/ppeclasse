<!DOCTYPE HTML>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
</head>

<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">

<div class="collapse navbar-collapse" id="navbarSupportedContent">

    <ul class="navbar-nav mr-auto">

        <li class="nav-item active">

            <a class="nav-link" href="http://51.178.86.117:8092/Login/">Accueil <span class="sr-only">(current)</span></a>

        </li>
            <li class="nav-item">

            <a class="nav-link" href="affichage.php">Utilisateurs</a>

        </li>

        <li class="nav-item">

            <a class="nav-link" href="login_create.php">Inscription</a>

        </li>

        <li class="nav-item">

            <a class="nav-link" href="http://51.178.86.117:8092/Login/">Connexion</a>

        </li>


        
    </ul>

</div>

</nav>
    <section class="container">
        <div class="row mb-5">
            <div class="col-lg-12">
                <h1 class="text-center">M2L</h1>
                <h4 class="text-center">Création d'un nouvel utilisateur</h4>
            </div>
        </div>
        <div class="row justify-content-md-center align-items-center">
            <div class="col-md-6">
                <?php

                try {
                    $db = new PDO('mysql:host=localhost:3306;dbname=ppe3_groupe2', 'kalil', 'Poh9oir4');
                    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING); // On émet une alerte à chaque fois qu'une requête a échoué.
                } catch ( Exception $e) {
                    die( 'Erreur  : ' . $e->getMessage() );
                }

                if( !empty( $_REQUEST['login']) && !empty( $_REQUEST['password'] ) ) {

                    $login = htmlentities( $_REQUEST['login'], ENT_QUOTES, 'UTF-8' );
                    $password = htmlentities( $_REQUEST['password'], ENT_QUOTES, 'UTF-8' );

                    $q = $db->prepare(
                            'SELECT COUNT(*) FROM users WHERE login=:login;'
                    );
                    $res = $q->execute([
                       'login'  => $login
                    ]);
                    $nbUser = current( $q->fetch( PDO::FETCH_ASSOC ) );
                    if( $nbUser == 0 ) {

                        $q = $db->prepare(
                                'DELETE FROM users WHERE ID=:ID;'
                        );
                        $res = $q->execute( [
                            ':login'    => $login,
                            ':password' => $password
                            ]);
                        if( !$res ) {
                            die( "Une erreur s'est produite sur l'execution de la requête" );
                        } else {
                            echo '<p class="lead">Votre compte a été enregistré !</p>';
                            echo '<p class="lead"><a class="btn btn-primary" href="login_index.php">Me connecter >></a> </p>';
                        }
                    } else {
                        echo '<p>Utilisateur déjà existant. Merci de recommencer.</p>';
                        echo '<p><a href="login_create.php" class="btn btn-primary">Réessayer >></a></p>';
                    }
                }

                ?>
            </div>
        </div>
    </section>


</body>

</html>