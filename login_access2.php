<!DOCTYPE HTML>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
</head>

<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">

<div class="collapse navbar-collapse" id="navbarSupportedContent">

    <ul class="navbar-nav mr-auto">

        <li class="nav-item active">

            <a class="nav-link" href="http://51.178.86.117:8092/Login/">Accueil <span class="sr-only">(current)</span></a>

        </li>
            <li class="nav-item">

            <a class="nav-link" href="affichage.php">Utilisateurs</a>

        </li>

        <li class="nav-item">

            <a class="nav-link" href="login_create.php">Inscription</a>

        </li>

        <li class="nav-item">

            <a class="nav-link" href="http://51.178.86.117:8092/Login/">Connexion</a>

        </li>


        
    </ul>

</div>

</nav>
    <section class="container">
        <div class="row mb-5">
            <div class="col-lg-12">
                <h1 class="text-center">GESTION DES DROITS</h1>
            </div>
        </div>
        <div class="row justify-content-md-center align-items-center">
            <div class="col-md-6">
                <?php


                try {
                    $db = new PDO('mysql:host=localhost:3306;dbname=ppe3_groupe2', 'jeanmarie', 'dai7ahh7');
                    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING); // On émet une alerte à chaque fois qu'une requête a échoué.
                } catch ( Exception $e) {
                    die( 'Erreur  : ' . $e->getMessage() );
                }

                if( !empty( $_REQUEST['login']) && !empty( $_REQUEST['password'] ) ) {

                    $login = htmlentities( $_REQUEST['login'], ENT_QUOTES, 'UTF-8' );
                    $password = htmlentities( $_REQUEST['password'], ENT_QUOTES, 'UTF-8' );

                    try {
                        $q = $db->prepare("SELECT login,password FROM users WHERE login=:login AND password=PASSWORD(:password);");

                    } catch ( Exception $e ) {
                        die( 'Erreur  : ' . $e->getMessage() );
                    }
                    $res = $q->execute( [
                        ':login'    => $login,
                        ':password' => $password
                        ]);
                    if( !$res ) {
                        die( "Une erreur s'est produite sur l'execution de la requête" );
                    } else {
                        $userInfos = $q->fetch(PDO::FETCH_ASSOC);
                        if( $userInfos ) {
                            echo '<p class="lead">Utilisateur : <b>' . $userInfos['login'] . '</b></p>';
                            echo '<p class="lead">Vous être à présent connecté à votre COMPTE !</p>';
                            echo '<p class="lead"><a class="btn btn-primary" href="login_index.php">Se déconnecter >></a> </p>';
                        } else {
                            echo '<p class="lead text-danger"><b>Vos login et mot de passe sont incorrect !</b></p>';
                            echo '<p class="lead"><a class="btn btn-primary" href="login_index.php">Réessayer >></a> </p>';
                        }
                    }
                    $q->closeCursor();
                }

                ?>
            </div>
        </div>
    </section>


</body>

</html>