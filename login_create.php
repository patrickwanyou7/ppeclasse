<!DOCTYPE HTML>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" rel="stylesheet">
</head>

<body>
<nav class="navbar navbar-expand-lg navbar-light bg-light">

<div class="collapse navbar-collapse" id="navbarSupportedContent">

    <ul class="navbar-nav mr-auto">

        <li class="nav-item active">

            <a class="nav-link" href="http://51.178.86.117:8092/Login/">Accueil <span class="sr-only">(current)</span></a>

        </li>
            <li class="nav-item">

            <a class="nav-link" href="affichage.php">Utilisateurs</a>

        </li>

        <li class="nav-item">

            <a class="nav-link" href="login_create.php">Inscription</a>

        </li>

        <li class="nav-item">

            <a class="nav-link" href="http://51.178.86.117:8092/Login/">Connexion</a>

        </li>


        
    </ul>

</div>

</nav>
    <section class="container">
        <div class="row mb-5">
            <div class="col-lg-12">
                <h1 class="text-center">M2L</h1>
                <h4 class="text-center">Création d'un nouvel utilisateur</h4>
            </div>
        </div>
        <div class="row justify-content-md-center align-items-center">
            <form class="col-lg-4" method="post" action="login_validate.php" name="loginCreateForm">
                <div class="form-group">
                    <label for="loginInput">Saisir un identifiant : </label>
                    <input type="text" class="form-control" name="login" placeholder="Identifiant" required>
                </div>
                <div class="form-group">
                    <label for="passwordInput">Saisir un mot de passe : </label>
                    <input type="password" class="form-control" name="password" placeholder="Mot de passe" required>
                </div>
                <button type="submit" class="btn btn-primary">Enregistrer</button>
                    <a href="http://51.178.86.117:8092/Login/" class="btn btn-success">retour a la page d'acceuil</a> <br>

            </form>
        </div>
    </section>

</body>

</html>